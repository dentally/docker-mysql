FROM ruby:2.6.3-slim
RUN apt-get update -y \
    && mkdir -p /usr/share/man/man1 \
    && mkdir -p /usr/share/man/man7 \
    && apt-get install -yqq --no-install-recommends \
      build-essential \
      default-libmysqlclient-dev \
      mariadb-client \
    && rm -rf /var/lib/apt/lists/*

RUN gem install mysql2 sequel csv ruby-rtf
